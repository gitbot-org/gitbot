const fetch = require('node-fetch')

// "@jaybot exec this $json[{text:'salut'}]"
// "@jaybot run this"
// "@jaybot I love JavaScript"
// `@jaybot exec this please $json[{"text":"salut la terre"}]`
// `@jaybot run JavaScript`

fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: "@jaybot I love JavaScript",
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("🖐️", data)

}).catch(error => {
  console.error("😡", error)
})


fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: `hey @jaybot ping`,
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("hey @jaybot ping 🖐️", data)

}).catch(error => {
  console.error("😡", error)
})

fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: `hey @jaybot pong`,
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("hey @jaybot pong 🖐️", data)

}).catch(error => {
  console.error("😡", error)
})




fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: "hello @jaybot do you want to play ping-pong?",
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("🖐️", data)

}).catch(error => {
  console.error("😡", error)
})



fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: "@jaybot what are you thinking about?",
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("🖐️", data)

}).catch(error => {
  console.error("😡", error)
})


fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: "@jaybot give me the weather please",
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("🖐️", data)

}).catch(error => {
  console.error("😡", error)
})

fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: "@jaybot say hello to the world",
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("🖐️", data)

}).catch(error => {
  console.error("😡", error)
})

fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: "@jaybot happy new year",
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("🖐️", data)

}).catch(error => {
  console.error("😡", error)
})
