It exists 2 licenses for the GitBot project

- the core license: see `./LICENSE.CORE.md`
- the enterprise license: see `./LICENSE.EE.md`

The enterprise license applies to the contents and usages of the contents of `./runtimes-ee` and `./plugins-ee`.


