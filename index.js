// TODO:
//   - add https support

// Require the framework and instantiate it
const fastify = require('fastify')({ logger: true })
const path = require('path')


// ===== [START] Initialize data =====
const config = require('./config')
// ===== [END] Initialize data =====


fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'public'),
  prefix: '/'
})

fastify.register(require('fastify-formbody'))

fastify.register(require('./routes/routes'), { // options
  config: "👋 this is the config"
})

// Run the server!
const start = async () => {
  try {
    await fastify.listen(config.serverHttpPort, config.serverIP)
    fastify.log.info(`server listening on ${fastify.server.address().port}`)

  } catch (error) {
    fastify.log.error(error)
    process.exit(1)
  }
}
start()
