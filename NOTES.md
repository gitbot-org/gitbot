

## TODO (publish the TODOs as issues)

- runtimes
  - publish source code of the runtime
  - update the kotlin code with https://k33g.gitlab.io/articles/2020-10-31-UPDATE-KOTLIN-RESULT.html
  - add ruby and python support
  - add kotlin support
- messages source
  - add GitLab support (and the reply to issue)
  - add GitHub support
  - add Slack support
- documentation
- website
- https
- GitLab
  - add GitLab helpers (to Javascript and to the runtime)
