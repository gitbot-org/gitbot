FROM gitpod/workspace-full

USER gitpod

# install GraalVM
RUN bash -c ". /home/gitpod/.sdkman/bin/sdkman-init.sh \
             && sdk install java 20.3.0.r11-grl"

RUN brew install kotlin
