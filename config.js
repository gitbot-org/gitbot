
// not used
const REDIS_URL = process.env.REDIS_URL || "redis://127.0.0.1"

const SERVER_IP = process.env.SERVER_IP || "0.0.0.0"
const SERVER_HTTP_PORT = parseInt(process.env.SERVER_HTTP_PORT || "8080")

// === 🦊 GitLab ===
const BOT_USER_NAME = process.env.BOT_USER_NAME || "jaybot"
const BOT_TOKEN = process.env.BOT_TOKEN || "bot_token"
const GITLAB_URL = process.env.GITLAB_URL || "https://gitlab.com"
const GITLAB_API_URL = `${GITLAB_URL}/api/v4`

//console.log("BOT_TOKEN:", BOT_TOKEN)

const DICTIONARY_PATH = process.env.DICTIONARY_PATH || "./rules/demo/.rules.yml"

// GraalVM runtime path
//const GRAALVM_RUNTIME_PATH = process.env.GRAALVM_RUNTIME_PATH || "./runtimes/funky-graalvm-runtime-1.0.0-SNAPSHOT-fat.jar"
const GRAALVM_RUNTIME_PATH = process.env.GRAALVM_RUNTIME_PATH || "./runtimes/gitbot-graalvm-rt-ce-1.0.0-SNAPSHOT-fat.jar"
const KOTLIN_RUNTIME_PATH = process.env.KOTLIN_RUNTIME_PATH || "./runtimes/gitbot-kotlin-rt-ce-1.0.0-SNAPSHOT-fat.jar"

// the ip server is the same as SERVER_IP
const GRAALVM_RUNTIME_SERVER_IP = process.env.GRAALVM_RUNTIME_SERVER_IP || "0.0.0.0"
const KOTLIN_RUNTIME_SERVER_IP = process.env.KOTLIN_RUNTIME_SERVER_IP || "0.0.0.0"

const PID_PATH = process.env.PID_PATH || "./pid"

exports.serverIP = SERVER_IP
exports.serverHttpPort = SERVER_HTTP_PORT
exports.redisUrl = REDIS_URL

exports.bot = { userName: BOT_USER_NAME, token: BOT_TOKEN }

exports.dictionary = {
  path: DICTIONARY_PATH
}

exports.runtimes = {
  graalVMPath: GRAALVM_RUNTIME_PATH,
  graalVMServerIP: GRAALVM_RUNTIME_SERVER_IP,
  kotlinPath: KOTLIN_RUNTIME_PATH,
  kotlinServerIP: KOTLIN_RUNTIME_SERVER_IP
}

exports.pidPath = PID_PATH

exports.gitLabAPIUrl = GITLAB_API_URL
