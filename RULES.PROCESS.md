# Rules

## GitLab rules

- Rules are defined in a yaml file (trigger and actions)
- On the GitLab side you need to define a hook to `/gitlab/message` (events: issues, merge_requests, notes on issues and on merge requests)
  - see `/routes/routes.js`
  - if the route is "called", the message is analyzed
    - `const analyze = require('../analysis/analysis.tools').analyze`

## Analysis

- see `/analysis/analysis.tools` and `analyze()``
  - when loading:
    - load the rules
    - start the services (kill services already running)

- when `analyze()` is called, for each rule, it check if it exist similarities with the message
  -
  - and then call (for every rule): `nlp.checkSimilarity(message, substSentence)`
    - see `const nlp = require('../nlp/nlp.tools')`

