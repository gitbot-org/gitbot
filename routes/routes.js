const config = require('../config')

const analyze = require('../analysis/analysis.tools').analyze
const answer = require('../answer/answer.tools')

async function routes (fastify, options) {

  fastify.log.info(`🤖 options:`, options)

  // source of the message is a simple json post request
  fastify.post(`/message`, async (request, reply) => {
    let botToken = config.bot.token // put in a environment variable
    let message = request.body

    if(message.text && message.from) { // message text comes from the user
      message.sender="rest"
      analyze(message, reply)

    } else { // if(message.text && message.from)
      answer.send(message.sender, reply, {error: "😡 message is malformed"})
    }

  })

  fastify.post(`/event`, async (request, reply) => {
    let message = request.body
    return { message: "🚧 WIP"}
  })

  fastify.post(`/gitlab/message`, async (request, reply) => {
    // 🖐️ the result can be triggered by issue or note or a merge request
    let received = request.body

    if((received.object_kind === "note" || received.object_kind === "issue" || received.object_kind === "merge_request") && received.user.username!==config.bot.userName) {

      let message = {
        sender: "gitlab",
        object_kind: received.object_kind,
        text: received.object_attributes.description,
        from: received.user.username,
        user: received.user,
        project: received.project,
        object_attributes: received.object_attributes,
        repository: received.repository,
        issue: received.issue,
        merge_request: received.merge_request,
        labels: received.labels,
        changes: received.changes
      }
      analyze(message, reply)
    } else { // if(message.text && message.from)
      answer.send(message.sender, reply, {error: "😡 message is malformed"})
    }
  })

  fastify.post(`/gitlab/event`, async (request, reply) => {
    let message = request.body
    return { message: "🚧 WIP"}
  })

  fastify.post(`/github/message`, async (request, reply) => {
    let message = request.body
    return { message: "🚧 WIP"}
  })

  fastify.post(`/github/event`, async (request, reply) => {
    let message = request.body
    return { message: "🚧 WIP"}
  })

  fastify.post(`/slack/message`, async (request, reply) => {
    let message = request.body
    return { message: "🚧 WIP"}
  })

}

module.exports = routes
