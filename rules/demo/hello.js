function main(params) {
  let info = JSON.parse(params)
  let userName = info.data.USER_NAME
  return `👋 @${userName} nice to meet you 😄`
}
