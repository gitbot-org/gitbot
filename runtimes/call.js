const fetch = require('node-fetch')

// "@jaybot exec this $json[{text:'salut'}]"
// "@jaybot run this"
// "@jaybot I love JavaScript"
// `@jaybot exec this please $json[{"text":"salut la terre"}]`
// `@jaybot run JavaScript`

fetch('http://127.0.0.1:9997', {
  method: 'post',
  body: JSON.stringify({
    name: "bob"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  console.log("🖐️", data)
  return data.text() // if python (and ruby) else if javascript : data.json()
}).then(data => {
  console.log("🖐️", data)

}).catch(error => {
  console.error("😡", error)
})

