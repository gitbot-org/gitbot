const { exec, spawn } = require('child_process')
const fs = require('fs')

try {
  // set environment variable for the JVM
  process.env.PORT = 9997
  process.env.LANG = "python"
  process.env.FUNCTION_CODE = fs.readFileSync("./samples/hello.py", 'utf8')

  // here or with a post ?
  process.env.BOT_TOKEN = "token"
  process.env.BOT_USER_NAME = "jay"

  let runtTimePath = "./gitbot-graalvm-rt-ce-1.0.0-SNAPSHOT-fat.jar"

  // This will inject the source code to the java runtime
  // and then start a new microservice

  // 🖐️ find another way
  let child = spawn(
    "java",
    ['-Dfile.encoding=UTF8', '-jar', runtTimePath],
    process.env
  )


  child.on('exit', _ => {
    console.log('🤖 [GraalVM] Function exited!')
  })

  child.stdout.on('data', (data) => {
    console.log(`🤖 [GraalVM] ${data}`)
  })

  child.stderr.on('data', (data) => {
    console.log(`😡 [GraalVM stderr] ${data}`)
  })
} catch (error) {
  console.error(`😡 [GraalVM when compling or starting] ${error}`)
  process.exit(1)
}
