import json

def main(params, context):
  returnedObject = {
    "message": f'Hello {params.getString("name")}.'
  }

  return json.dumps(returnedObject)
