import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.ext.web.RoutingContext
import io.vertx.core.json.JsonObject

fun main(params: JsonObject, context: RoutingContext): Any {

  return json {
    obj(
      "message" to "Hello World!!!",
      "total" to 42,
      "name" to params.getString("name")
    )
  }.encodePrettily()
}
