function message() {
  return "Hello World!"
}

function main(params) {

  return JSON.stringify({
    message: message(),
    total: 42,
    authors: ["@k33g_org"],
    context: "this is a demo",
    params: params.getString("name")
  })
}


