const natural = require('natural')

//TODO:
/*
 - exact match
 - start with
 - sentiments
*/

/**
* ? similarity triggers
# --- NLP ---
# Jaro-Winkler: value jaroWinkler
#   best similarity -> 1
#   worst similarity -> 0
#
# Levenshtein: value levenshtein
#   best similarity -> 0
#   worst similarity -> N
#
# Dice Coefficient: value diceCoefficient
# diceCoefficient > similarityTrigger (0.75)
*/

function tokenize(text) {
    let tokenizer = new natural.WordTokenizer()
    let tokens = tokenizer.tokenize(text)
    return tokens
}

function jaroWinklerDistance(text, ruleSentence, similarityTrigger=0.8) {
  let distance = natural.JaroWinklerDistance(text, ruleSentence)
  return {
    distance: distance,
    isSimilar: distance >= similarityTrigger, // ! >= or > and never use 1 but 0.99 => check the documentation of nlp
    kind: "jaroWinklerDistance"
  }
}

function levenshteinDistance(text, ruleSentence, similarityTrigger=5) {
  let distance = natural.LevenshteinDistance(text, ruleSentence)
  return {
    distance: distance,
    isSimilar: distance <= similarityTrigger,
    kind: "levenshteinDistance"
  }
}

function diceCoefficient(text, ruleSentence, similarityTrigger=0.75) {
  let coefficient = natural.DiceCoefficient(text, ruleSentence)
  return {
    coefficient: coefficient,
    isSimilar: coefficient > similarityTrigger,
    kind: "diceCoefficient"
  }
}


//TODO: define a method to use in the configuration file
function checkSimilarity(message, substSentence, similarityTrigger) {
  if(similarityTrigger==undefined || similarityTrigger==null) {
    //console.log("🟢 use default ", message.text, substSentence)
    return jaroWinklerDistance(message.text, substSentence)
  } else {
    //console.log("🟠 use ", similarityTrigger, message.text, substSentence)
    return jaroWinklerDistance(message.text, substSentence, similarityTrigger)
  }

  //return levenshteinDistance(message.text, substSentence, rule.nlp.similarityTrigger)
  //return levenshteinDistance(message.text, substSentence)
  //return diceCoefficient(message.text, substSentence, rule.nlp.similarityTrigger)
  //return diceCoefficient(message.text, substSentence)
  //return jaroWinklerDistance(message.text, substSentence, rule.nlp.similarityTrigger)

}

module.exports = {
  tokenize: tokenize,
  jaroWinklerDistance: jaroWinklerDistance,
  levenshteinDistance: levenshteinDistance,
  diceCoefficient: diceCoefficient,
  checkSimilarity: checkSimilarity
}
