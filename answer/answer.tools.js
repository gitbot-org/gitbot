const config = require('../config')
const gitlab = require('../gitlab/gitlab.tools')

function send(message, reply, jsonMessage) {
  if(message.sender==="rest") {
    reply.send(jsonMessage)
  }

  if(message.sender==="gitlab") {
    console.log("==[🔵 sending to GitLab 🦊]===============================")

    if(message.object_kind==="note" && message.issue!==undefined) {

      gitlab.addNoteToIssue(
        message.project.id,
        message.issue.iid,
        jsonMessage.message
      )
      .then(data => {
        reply.send(data)
        return data
      })
      .catch(error => {
        console.error(`😡 ${error}`)
      })
    }

    if(message.object_kind==="note" && message.merge_request!==undefined) {

      gitlab.addNoteToMergeRequest(
        message.project.id,
        message.merge_request.iid,
        jsonMessage.message
      )
      .then(data => {
        return data
      })
      .catch(error => {
        console.error(`😡 ${error}`)
      })
    }

    if(message.object_kind==="issue") {

      gitlab.addNoteToIssue(
        message.project.id,
        message.object_attributes.iid,
        jsonMessage.message
      )
      .then(data => {
        return data
      })
      .catch(error => {
        console.error(`😡 ${error}`)
      })

    }

    /*
    if(message.object_kind=="merge_request") {
      console.log(message.object_attributes.iid)
      console.log(message.project.id)
      console.log(message.object_kind)
      // TODO
    }
    */

    //TODO: post to GitLab
    console.log("=========================================================")
    reply.send({status:"OK"})
  }
}

module.exports = {
  send: send
}
