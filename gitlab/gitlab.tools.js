const config = require('../config')
const fetch = require('node-fetch')

let baseUri = config.gitLabAPIUrl
let token = config.bot.token
let headers = {
  "Content-Type": "application/json",
  "Private-Token": token
}

function addNoteToIssue(projectId, issueInternalId, body) {
  let path = `/projects/${projectId}/issues/${issueInternalId}/notes`

  return fetch(`${baseUri}${path}`, {
    method: 'POST',
    body: JSON.stringify({body:body}),
    headers: headers
  })
  .then(res => res.json())
  .then(json => {
    console.log("🦊 return from GitLab", json)
  })
  .catch(error => {
    return error
  })
}

function addNoteToMergeRequest(projectId, mergeRequestInternalId, body) {

  let path = `/projects/${projectId}/merge_requests/${mergeRequestInternalId}/notes`
  return fetch(`${baseUri}${path}`, {
    method: 'POST',
    body: JSON.stringify({body:body}),
    headers: headers
  })
  .then(res => res.json())
  .then(json => {
    console.log("🦊 return from GitLab", json)
  })
  .catch(error => {
    return error
  })
}

module.exports = {
  addNoteToIssue: addNoteToIssue,
  addNoteToMergeRequest: addNoteToMergeRequest
}
