const fetch = require('node-fetch')

function fetchMessageFromService(url, augmentedData, message) {
  console.log("===[augmentedData & message]======================")
  console.log(augmentedData)
  console.log(message)
  console.log("=================================================")

  return fetch(url, {
    method: 'POST',
    body: JSON.stringify({data: augmentedData, message: message}),
    headers: { 'Content-Type': 'application/json' }
  })
  .then(res => {
    return res.text()
  })
}


module.exports = {
  fetchMessageFromService: fetchMessageFromService
}
