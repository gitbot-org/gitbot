const { addYamlVariablesToData } = require('./languages')
const { fetchMessageFromService } = require('./services.helpers')
const answer = require('../answer/answer.tools')
const config = require('../config')

function ifRuby(rule, data, message, reply) {
  if(rule.ruby!==undefined){
    let httpPort = rule.ruby.port
    let augmentedData = addYamlVariablesToData(rule.variables, data)

    // call the GraalVM microservice
    fetchMessageFromService(
      `http://${config.runtimes.graalVMServerIP}:${httpPort}`,
      augmentedData,
      message
    )
    .then(text => {
      console.log(`🟢 return from Ruby`, text)
      answer.send(message, reply, {message: text})
    })
    .catch(error => {
      console.log(`🔴 return from Ruby`, error)
      answer.send(message, reply, {error: error})
    })

  } else {
    console.log("🤖 no ruby")
  }
}

module.exports = {
  ifRuby: ifRuby
}
