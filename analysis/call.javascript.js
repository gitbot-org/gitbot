const { addYamlVariablesToData } = require('./languages')
const { fetchMessageFromService } = require('./services.helpers')
const answer = require('../answer/answer.tools')
const config = require('../config')


function ifJavaScript(rule, data, message, reply) {
  if(rule.javascript!==undefined){
    // 🚧 WIP
    let httpPort = rule.javascript.port

    let augmentedData = addYamlVariablesToData(rule.variables, data)

    // call the GraalVM microservice
    fetchMessageFromService(
      `http://${config.runtimes.graalVMServerIP}:${httpPort}`,
      augmentedData,
      message
    )
    .then(text => {
      console.log(`🟢 return from JavaScript`, text)
      answer.send(message, reply, {message: text})
    })
    .catch(error => {
      console.log(`🔴 return from JavaScript`, error)
      answer.send(message, reply, {error: error})
    })

  } else {
    console.log("🤖 no javascript")
  }
}

module.exports = {
  ifJavaScript: ifJavaScript
}
