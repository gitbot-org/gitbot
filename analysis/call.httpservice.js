const { addYamlVariablesToData } = require('./languages')
const { fetchMessageFromService } = require('./services.helpers')
const answer = require('../answer/answer.tools')

function ifHttpService(rule, data, message, reply) {
  if(rule["http-service"]!==undefined){
    let url = rule["http-service"]

    let augmentedData = addYamlVariablesToData(rule.variables, data)

    // call the external http service
    fetchMessageFromService(
      url,
      augmentedData,
      message
    )
    .then(text => {
      console.log(`🟢 return from http service`, text)
      answer.send(message, reply, {message: text})
    })
    .catch(error => {
      console.log(`🔴 return from http service`, error)
      answer.send(message, reply, {error: error})
    })

  } else {
    console.log("🤖 no http-service")
  }
}

module.exports = {
  ifHttpService: ifHttpService
}
