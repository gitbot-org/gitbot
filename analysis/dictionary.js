const yaml = require('js-yaml')
const fs = require('fs')
const config = require('../config')


function loadRules(yamlFile) {
    try {
      // 🖐️ synchronous load
      let content = yaml.safeLoad(fs.readFileSync(yamlFile, 'utf8'));
      return content
    } catch(error) {
      console.error(`😡 there was a problem when loading ${yamlFile}`)
      console.error(error.name, error.message)
      return null
    }
}

function getRules() {
  let rulesDictionary = loadRules(config.dictionary.path)
  if(rulesDictionary !== null) {
    //console.log(JSON.stringify(rulesDictionary, null, 2))
  } else {
    process.exit(1)
  }
  return rulesDictionary.rules
}

module.exports = {
  loadRules: loadRules,
  getRules: getRules
}
