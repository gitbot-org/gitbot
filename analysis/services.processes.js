const config = require('../config')
const fs = require('fs')

function killServices() {
  // kill at start the existing microservices
  try {
    if (fs.existsSync(`${config.pidPath}/pid.json`)) {
      // file exists
      // kill the running pid(s) (if exist)
      let pids = JSON.parse(fs.readFileSync(`${config.pidPath}/pid.json`, 'utf8'))
      try {
        pids.forEach(item => {
          process.kill(item.pid)
          console.log(`👋 ${item.pid} killed`)
        })
      } catch(error) {
        console.error(`😡 [JVM](pid.json)(kill) ${error}`)
      }
    }
  } catch(error) {
    console.error(`😡 [JVM](pid.json) ${error}`)
  }
}

function storeListOfServices(JVMServices) {
  fs.writeFile(`${config.pidPath}/pid.json`, JSON.stringify(JVMServices), error =>{
    if(error) {
      console.error(`😡 [JVMServices] ${error}`)
      return
      //process.exit(1)
    }
    console.log("🤖 [JVMServices] the file is saved!")
  })
}

module.exports = {
  killServices: killServices,
  storeListOfServices: storeListOfServices
}
