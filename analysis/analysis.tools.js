const config = require('../config')
const nlp = require('../nlp/nlp.tools')
const dictionary = require('./dictionary')
const text = require('../text/text.tools')
const answer = require('../answer/answer.tools')
const { killServices, storeListOfServices}  = require('./services.processes')
const { startJVMServices } = require('./services.jvm')
const { ifJavaScript } = require('./call.javascript')
const { ifRuby } = require('./call.ruby')
const { ifPython } = require('./call.python')
const { ifKotlin } = require('./call.kotlin')
const { ifHttpService } = require('./call.httpservice')

// Load the rules
let rules = dictionary.getRules()
// kill at start the existing microservices
killServices()
// then start the services
let JVMServices = startJVMServices(rules)

// keep a list of processes
storeListOfServices(JVMServices)

// add some info (to be passed to the services)
let memoryData = { // where I use it? => analyze()
  BOT_USER_NAME: config.bot.userName
  // => 🖐️ don't add any token here, only transfer it from environment variables
  // if external services need the bot token, you need to do the same think
}

/*
  message is sent from a note (for example): "@jay how are you?"
  substSentence is the sentence of a rule after variable substitution
*/
function findSimilarities(rules, message, data) { // parse the rules
  let similarities = [] // you can get several response
  for(var member in rules) {
    let rule = rules[member]
    let sentence = rule.sentence

    let substSentence = text.substituteVariables(sentence, data)

    // ! read the similarity trigger: `similarityTrigger`
    // ? right now I use only `jaroWinklerDistance`
    let similarity = nlp.checkSimilarity(message, substSentence, rule.similarityTrigger)
    if(similarity) {
      if(similarity.isSimilar) {
        similarities.push({
          rule, message, substSentence, similarity
        })
      }
    }
  } // end of for
  console.log("==[similarities]===============================")
  console.log(similarities)
  console.log("===============================================")
  return similarities
}

function analyze(message, reply) {
  // 🔋 === add info to data ===
  let data = Object.assign({}, memoryData)
  data.USER_NAME = message.from // I don't know if it's the same with GitLab, GitHub, Slack, etc ... (do the map on routes.js)
  data.MESSAGE_TEXT = message.text

  /**
   * TODO: add startsWith, contains
   * ! in this case replace "sentence" by "startsWith" or "contains" in the rule definition
   * ! and use tokenize
   * ? exact words: use a similarity trigger of 1 (default is 0.8)
   */
  /*
  rules:
    ping:
      sentence: "hey @${data.BOT_USER_NAME} ping"
      similarityTrigger: 1 # default is 0.8 if 1 it's an exact match
      javascript:
        port: 9090
        code: |
          function main(params) {
            let options = JSON.parse(params)
            return `🏓 pong ${options.data.USER_NAME}`
          }
  */

  // === parse the rules and find similarity(ies) with the message text ===
  let similarities = findSimilarities(rules, message, data)

  // === execute action(s) if we found similarity(ies) ===
  if(similarities.length > 0) {

    let similarity = similarities.length>1
      ? similarities.find(item => item.similarity.distance == Math.max.apply(Math, similarities.map(item=>item.similarity.distance)))
      : similarities[0]

    console.log("==[🟢 selected similarity]===============================")
    console.log(similarity)
    console.log("======================================================")

    let rule = similarity.rule  // use the rule of the first similarity

    ifJavaScript(rule, data, message, reply)
    ifRuby(rule, data, message, reply)
    ifPython(rule, data, message, reply)
    ifHttpService(rule, data, message, reply)
    ifKotlin(rule, data, message, reply)

  } else { //if(similarities.length <= 0)
    answer.send(message, reply, {message: "🤔 I'm sorry, but I don't understand"})
  }
}

module.exports = {
  analyze: analyze,
}
