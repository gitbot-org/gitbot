const { addYamlVariablesToData } = require('./languages')
const { fetchMessageFromService } = require('./services.helpers')
const answer = require('../answer/answer.tools')
const config = require('../config')

function ifPython(rule, data, message, reply) {
  if(rule.python!==undefined){
    // 🚧 WIP
    let httpPort = rule.python.port
    let augmentedData = addYamlVariablesToData(rule.variables, data)

    // call the GraalVM microservice
    fetchMessageFromService(
      `http://${config.runtimes.graalVMServerIP}:${httpPort}`,
      augmentedData,
      message
    )
    .then(text => {
      console.log(`🟢 return from Python`, text)
      answer.send(message, reply, {message: text})
    })
    .catch(error => {
      console.log(`🔴 return from Python`, error)
      answer.send(message, reply, {error: error})
    })

  } else {
    console.log("🤖 no python")
  }
}

module.exports = {
  ifPython: ifPython
}
