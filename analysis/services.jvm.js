const findTheUsedLanguage = require('./languages').findTheUsedLanguage
const config = require('../config')
const { spawn } = require('child_process')

//================================================
// run the microservices (compilation and start)
//================================================
function startJVMServices(rules) {
  let JVMServices = []

  for(var member in rules) {
    let rule = rules[member]
    let languageOptions = findTheUsedLanguage(rule)
    if(languageOptions.httpPort>0) {
      try {
        // set environment variable for the JVM
        process.env.PORT = languageOptions.httpPort
        process.env.LANG = languageOptions.language
        process.env.FUNCTION_CODE = languageOptions.sourceCode

        // all tokens are transfered to service with environment variables
        process.env.BOT_TOKEN = config.bot.BOT_TOKEN
        process.env.BOT_USER_NAME = config.bot.BOT_USER_NAME

        let runtTimePath = languageOptions.runtTimePath // (graalVM or kotlin)
        //config.runtimes.graalVMPath

        // This will inject the source code to the java runtime
        // and then start a new microservice

        // 🖐️ find another way
        let child = spawn(
          "java",
          ['-Dfile.encoding=UTF8', '-jar', runtTimePath],
          process.env
        )

        JVMServices.push({
          ruleName: member,
          httpPort: languageOptions.httpPort,
          language: languageOptions.language,
          //source: languageOptions.sourceCode,
          pid: child.pid
        })

        child.on('exit', _ => {
          console.log('🤖 [JVM] Function exited!')
        })

        child.stdout.on('data', (data) => {
          console.log(`🤖 [JVM] ${data}`)
        })

        child.stderr.on('data', (data) => {
          console.log(`🤖 [JVM.] ${data}`)
        })

      } catch (error) {
        console.error(`😡 [JVM when compling or starting] ${error}`)
        process.exit(1)
      }
    }
  } // end: for(var member in rules)
  console.log("==[JVM MicroServices]===================")
  console.log(JVMServices)
  console.log("===============================================")

  return JVMServices
}


module.exports = {
  startJVMServices: startJVMServices
}
