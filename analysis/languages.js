const fs = require('fs')
const config = require('../config')

/* findTheUsedLanguage:
  determine from the rule the used language
  return { sourceCode, language, httpPort }
*/

function findTheUsedLanguage(rule) {
  let languages = {
    javascript: "js", python: "python", ruby: "ruby", kotlin: "kotlin"
  }
  var sourceCode = ""
  var language = ""
  var httpPort = 0

  for (var member in languages) {

    if(rule[member]!==undefined) { // member could be == "javascript", "python", "ruby", "kotlin"

      httpPort = rule[member].port
      language = languages[member]

      if(rule[member].code!==undefined) {
        sourceCode = rule[member].code
      } else {

        // Load external script
        if(rule[member].include!==undefined) {
          try {
            sourceCode = fs.readFileSync(rule[member].include, 'utf8')
          } catch(error) {
            console.error(`😡 [Reading external file] ${error}`)
            process.exit(1)
          }
        } else {
            console.error(`😡 [no source code nor external script] ${error}`)
            process.exit(1)
        }
      }
    } // end of if(rule[member]!==undefined)
  } // end of for (var member in languages)
  let runtTimePath = language==="kotlin"
                      ? config.runtimes.kotlinPath
                      : config.runtimes.graalVMPath

  return { sourceCode, language, httpPort, runtTimePath}
}

function addYamlVariablesToData(ruleVariables, data) {
  // 🖐️ try to do the same thing with assign
  for(var member in ruleVariables) {
    data[member] = ruleVariables[member]
  }
  return data
}

module.exports = {
  findTheUsedLanguage: findTheUsedLanguage,
  addYamlVariablesToData: addYamlVariablesToData
}
