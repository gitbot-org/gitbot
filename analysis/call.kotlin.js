const { addYamlVariablesToData } = require('./languages')
const { fetchMessageFromService } = require('./services.helpers')
const answer = require('../answer/answer.tools')
const config = require('../config')


function ifKotlin(rule, data, message, reply) {
  if(rule.kotlin!==undefined){
    let httpPort = rule.kotlin.port

    let augmentedData = addYamlVariablesToData(rule.variables, data)

    // call the GraalVM microservice
    fetchMessageFromService(
      `http://${config.runtimes.kotlinServerIP}:${httpPort}`,
      augmentedData,
      message
    )
    .then(text => {
      console.log(`🟢 return from Kotlin`, text)
      answer.send(message, reply, {message: text})
    })
    .catch(error => {
      console.log(`🔴 return from Kotlin`, error)
      answer.send(message, reply, {error: error})
    })

  } else {
    console.log("🤖 no kotlin")
  }
}

module.exports = {
  ifKotlin: ifKotlin
}
