/*
Example:
let data = {BOT_USER_NAME:"jay", USER_NAME: "k33g"}
let sentence = "hey @${data.BOT_USER_NAME} I'm @${data.USER_NAME} 😉"
*/

function substituteVariables(sentence, data) { // it substitute the variables only starting with data. (prefixed)
  var newSentence = sentence
  for(var member in data) {
    newSentence=newSentence.split("${data."+member+"}").join(data[member])
  }
  return newSentence
}

module.exports = {
  substituteVariables: substituteVariables
}
