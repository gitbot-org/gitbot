const fetch = require('node-fetch')


fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: `hey @jaybot ping`,
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("hey @jaybot ping 🖐️", data)

}).catch(error => {
  console.error("😡", error)
})


fetch('http://127.0.0.1:8080/message', {
  method: 'post',
  body: JSON.stringify({
    text: `hey @jaybot pong`,
    from: "k33g"
  }),
  headers: {'Content-Type': 'application/json'}
}).then(data => {
  return data.json()
}).then(data => {
  console.log("hey @jaybot pong 🖐️", data)

}).catch(error => {
  console.error("😡", error)
})

