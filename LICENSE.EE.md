The GitBot Enterprise Edition (EE) license (the “EE License”)
Copyright (c) 2020-present Bots.Garden

With regard to the GitBot Software:

This software and associated documentation files (the "Software") may only be
used in production, if you (and any entity that you represent) have agreed to,
and are in compliance with, the GitBot Subscription Terms of Service, available
right now on demand at philippe.charriere@bots.garden (the “EE Terms”), or other
agreement governing the use of the Software, as agreed by you and Bots.Garden,
and otherwise have a valid GitBot Enterprise Edition subscription for the
correct number of GitBot instances. Subject to the foregoing sentence, you are free to
modify this Software and publish patches to the Software. You agree that Bots.Garden
and/or its licensors (as applicable) retain all right, title and interest in and
to all such modifications and/or patches, and all such modifications and/or
patches may only be used, copied, modified, displayed, distributed, or otherwise
exploited with a valid GitBot Enterprise Edition subscription for the  correct
number of GitBot instances.  Notwithstanding the foregoing, you may copy and modify
the Software for development and testing purposes, without requiring a
subscription.  You agree that Bots.Garden and/or its licensors (as applicable) retain
all right, title and interest in and to all such modifications.  You are not
granted any other rights beyond what is expressly stated herein.  Subject to the
foregoing, it is forbidden to copy, merge, publish, distribute, sublicense,
and/or sell the Software.

This EE License applies only to the part of this Software that is not
distributed as part of GitBot Core Edition (CE). Any part of this
Software distributed as part of GitBot CE, in whole or in part, is copyrighted under
the MIT Expat license. The full text of this EE License shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the GitBot Software, those
components are licensed under the original license provided by the owner of the
applicable component.

> The enterprise license applies to the contents and usages of the contents of `./runtimes-ee` and `./plugins-ee`.
