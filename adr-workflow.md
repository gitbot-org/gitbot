
at start: load rules (Dictionary)

message (eg: "hey @jay how are you?") --> POST /message

message {
  text
  from
}

for each **sentence** of the dictionary ->
  calculate the distance between **sentence** and **message.text**
